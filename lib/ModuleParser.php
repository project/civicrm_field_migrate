<?php
namespace CiviCrmFieldMigrate\lib;

class ModuleParser {
  public $files = array();
  public $fields = array();
  public $module = "";
  public $drupalRoot = "";
  public $fileExtensions = array(
    'module',
    'inc',
    'php',
    'css',
    'js',
    'tpl',
  );

  public static $drupalExtensions;

  function __construct($module, $drupal_root) {
    $this->module = $module;
    $this->drupalRoot = $drupal_root;
  }

  public function parseFiles(&$errors) {
    $info = self::$drupalExtensions[$this->module];
    $relative_path = str_replace('/' . $this->module . '.module', '', $info->uri);
    $path = $this->drupalRoot . '/' . $relative_path;

    $iterator = new \RecursiveIteratorIterator(
      new \RecursiveDirectoryIterator($path),
      \RecursiveIteratorIterator::SELF_FIRST);

    $objects = new \RegexIterator(
      $iterator,
      '/^.+\.(?:' . implode("|", $this->fileExtensions) . ')$/i',
      \RecursiveRegexIterator::GET_MATCH);

    foreach($objects as $name => $object){
      $this->files[] = str_replace(DRUPAL_ROOT . '/', '', $name);
    }

    return $this->files;
  }
}
