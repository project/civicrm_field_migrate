<?php
namespace CiviCrmFieldMigrate\lib;

class CiviCrmFieldMigrateController {
  /**
   * List of module names to update code in.
   * This gets populated in the validate() process.
   *
   * @var array
   */
  public $modules = array();

  public static $updatedMarker = '[[[[|]]]]';

  /**
   * Path to field mapping file.
   *
   * @var string
   */
  public $targetDefinitionPath = "";

  /**
   * The path to the field definition.
   * @var string
   */
  public $sourceDefinitionPath = "";

  /**
   * The target site's Drupal root path on the system.
   * @var string
   */
  public $drupalRoot = "";

  /**
   * Array of FieldParser keyed on the file path.
   * This gets populated in the validate() process.
   *
   * @var array
   */
  public $fields = array();

  /**
   * Flag to only run the validation process.
   *
   * @var bool
   */
  public $testOnly = false;

  /**
   * The custom fields in the current Drupal site. Should be provided in the constructor.
   *
   * @var array An array keyed on the numberic ID of the custom field with a string value of the machine name.
   */
  public $currentSiteCustomFields = array();

  /**
   * The loaded contents of the mapping file. Used to match local to target site fields.
   * @var array This gets set in the validate() process.
   */
  public $targetSiteCustomFields = array();

  /**
   * Null if validation has not been executed, true if successfully validated, false if failed validation.
   *
   * @var bool
   */
  private $_validated;

  /**
   * A flattened list of custom field names found in all the parsers.
   * This is populated in the validate() process.
   *
   * @var array
   */
  private $_uniqueFieldNames = array();

  /**
   * The all modules in the current Drupal site.
   *
   * @var array
   */
  public static $drupalModules = array();

  function __construct($modules, $target_source, $drupal_root, $current_site_custom_fields, $source_definition_path, $test_only = false) {
    $this->modules = array_flip($modules);
    $this->targetDefinitionPath = $target_source;
    $this->drupalRoot = $drupal_root;
    $this->testOnly = $test_only;
    $this->sourceDefinitionPath = $source_definition_path;

    // either we'll be getting the source fields from a definition file or from the current site.
    if(empty($this->sourceDefinitionPath) && empty($current_site_custom_fields)) {
      throw new \Exception('Cannot create instance of CiviCrmFieldMigrateController with an empty $current_site_custom_fields array.');
    }

    $this->currentSiteCustomFields = $current_site_custom_fields;
  }

  public function execute(&$errors, &$migrated_fields) {

    // A lot of important bootstrapping happens in the validation process.
    // Verify that "validation" has ran. Note that we're checking for "null", not "false".
    if(!isset($this->_validated)) {
      $this->validate($errors);
    }

    if($this->_validated === false) {
      $errors[] = 'Failed validation, CiviCRM Field Migration can not continue.';
      return false;
    }

    //  Find and replace references for source fields with target site fields.
    $migrated_fields = $this->updateFiles();

    return true;
  }

  private function updateFiles() {
    $fields_migrated = array();
    // We want to make sure everything has been validated before we started changing files.
    if($this->_validated !== true) {
      throw new \Exception("Unable to update files, the validation process has not been successfully completed.");
    }

    // Loop over files.
    foreach($this->fields as $src => $parser) {
      // If this parser found fields, load up the file's contents and lets start replacing the mapped fields.
      if(!empty($parser->fields)) {
        $contents = file_get_contents($this->drupalRoot . '/' . $src);
        if(empty($contents)) {
          // Skip this file if weren't able to get its contents.
          continue;
        }
        // Replace source fields with target site fields.
        foreach($parser->fields as $field_id) {
          // We need to get the field "name" from the "custom_id" so we can find a match in the target site's field.
          $field_name = $this->currentSiteCustomFields[$field_id];
          $new_field_id = array_search($field_name, $this->targetSiteCustomFields);

          // Is the field changing?
          if($new_field_id != $field_id) {
            // Add it to the fields being migrated.
            $fields_migrated[$field_id] = array(
              'id' => $field_id,
              'name' => $field_name,
              'new_id' => $new_field_id,
            );
          }

          // Replace current occurrences of custom_xxx with the matched one in the new site.
          $contents = self::replaceFieldIdentifier(
            $field_id,
            $new_field_id,
            $contents);

          // Replace current occurrences of custom-xxx with the matched one in the new site.
          $contents = self::replaceFieldIdentifier(
            str_ireplace('_', '-',$field_id),
            str_ireplace('_', '-', $new_field_id),
            $contents);

          // Attempt to replace the named version (myfield_123) of the file also
          $orig_id = str_ireplace('custom_', '', $field_id);
          $origin_name = strtolower($field_name . '_' . $orig_id);

          $mapped_id = str_ireplace('custom_', '', $new_field_id);
          $mapped_name = strtolower($this->targetSiteCustomFields[$new_field_id] . '_' . $mapped_id);

          $contents = self::replaceFieldIdentifier($origin_name, $mapped_name, $contents);

        }

        if(!$this->testOnly) {
          // The field replacement process adds markers to keep from updating a field multiple times.
          // Clean the file before we write it back to disk.
          $contents = self::cleanMarkers($contents);

          // Write the updated contents back to the file.
          file_put_contents($this->drupalRoot . '/' . $src, $contents);
        }
      }
    }

    return $fields_migrated;
  }

  /**
   * Replaces the original field with the new field.
   * Also appends a marker that keeps fields from being updated multiple times.
   *
   * @param string $old_identifier The original field identifier that will be updated.
   * @param string $new_identifier The new field identifier.
   * @param string $content The content to perform the find and replace on.
   * @return string The updated $content
   */
  private static function replaceFieldIdentifier($old_identifier, $new_identifier, $content) {
    // Make the marker regex safe.
    $marker = preg_quote(self::$updatedMarker);
    // Make the old_identifier regex safe.
    $old_identifier = preg_quote($old_identifier);
    // Replace occurrences of the old_identifier that is not followed by our marker with the new_identifier.
    $content = preg_replace(
      '/(?>' . $old_identifier . ')(?!' . $marker . ')/',
      ($new_identifier . self::$updatedMarker),
      $content);

    return $content;
  }

  private static function cleanMarkers($content) {
    return str_replace(self::$updatedMarker, '', $content);
  }

  /**
   * Drush's help information callback.
   *
   * @param string $command
   * @return string
   */
  public static function helpInfo($command) {
    switch($command) {
      case 'drush:civicrm-field-migrate':
        return 'Converts all references of CiviCRM\'s custom fields (i.e.: custom_100) to the target machines equivalent by referencing a mapping table. Either the --source or --target option is required.';
        break;
    }
  }


  /**
   * This Drush command's information to be used in Drush's hook_command.
   *
   * @return array
   */
  public static function commandInfo() {
    $items = array(
      'civicrm-field-migrate' => array(
        'description' => 'Converts all references of custom_xxx (CiviCRM\'s custom fields) in the provided modules code to use the equivalent version as found in the provided --source file.
      This allows migrating from one CiviCRM environment to another where custom fields may have different incremental IDs.
      Please note: this changes code only, a good example is exported views or Features.',
        'bootstrap' => DRUSH_BOOTSTRAP_NONE,
        'options' => array(
          'target' => array(
            'example-value' => '/some/path/production.fields.php',
            'description' => 'The mapping file that will be used to translate existing references of custom CiviCRM fields in the provided --modules files.',
            'required' => true,
          ),
          'modules' => array(
            'example-value' => 'my_custom_module, my_feature',
            'description' => 'The modules that will have their code changed to use the target CiviCRM\'s custom field IDs referenced in the --target file.',
            'required' => true,
          ),
          'source' => array(
            'example-value' => '/some/path/dev.fields.php',
            'description' => 'Instead of getting field definitions from the current Drupal site, reference this provided file.',
            'required' => false,
          ),
          'test-only' => 'Provide the --test-only option just to verify the field mapping integrity, no action is actually performed.',
        ),
        'aliases' => array(
          'cfm'
        ),
        'examples' => array(
          'Sample' => 'drush cfm --modules="my_custom_module, exported_feature_one" --target=/path/developer-1-civi.fields.php',
          'Test only' => 'drush cfm --test-only --modules="my_custom_module, exported_feature_one" --source=/path/developer-1-civi.fields.php',
        ),
        //'core' => '7.x',
        //'callback' => 'civicrm_field_migrate',
        'command-hook' => 'field_migrate'
      ),
    );

    return $items;
  }

  /**
   * Validates the provided drush options and verifies all needed module files are writable.
   *
   * @param array $errors An array or error messages if validation failed.
   * @return bool Returns true if valid, false if not.
   */
  public function validate(&$errors) {
    $this->validateOptions($errors);

    if(empty(self::$drupalModules)) {
      $errors[] = "The current Drupal site's modules was not provided. Can not continue.";
      return false;
    }

    $this->validateParsers($errors);

    // Get a flat list of all the parser's files. Also make sure files aren't listed twice
    // caused by potentially nested modules crawled by our recursive walked for each provided module name.
    $this->fields = array_flip(self::getUniqueFileList($this->modules));

    // Loop over the files and set a FieldParser for each.
    foreach($this->fields as $file_name => &$field_parser) {
      $field_parser = new FieldParser($file_name, $this->currentSiteCustomFields);
      $field_parser->parseFields($errors);
    }

    $this->_uniqueFieldNames = self::getUniqueCustomFieldNames($this->fields);

    // Verify there is metadata for each found CiviCRM custom field in the current drupal site.
    // First check for syntax "custom_xxx".
    $missing_source_fields = array_diff($this->_uniqueFieldNames, array_keys($this->currentSiteCustomFields));

    foreach($missing_source_fields as $missing_field) {
      $missing_field_name = $this->targetSiteCustomFields[$missing_field];
      $errors[] = 'Code references field: "' . $missing_field . '|' . $missing_field_name . '" but was not found in the current site\'s CiviCRM custom fields.';
    }

    $this->_uniqueFieldNames = array_flip($this->_uniqueFieldNames);
    foreach($this->_uniqueFieldNames as $id => &$name) {
      $name = $this->currentSiteCustomFields[$id];
    }

    // Verify there is an equivalent field in the mapping file.
    $missing_fields = array_diff($this->_uniqueFieldNames, $this->targetSiteCustomFields);
    if(!empty($missing_fields)) {
      foreach($missing_fields as $fld => $name) {
        // Skip fields that are in the $missing_source_fields because they are obviously broken and don't need to be reported twice.
        if(array_search($fld, $missing_source_fields) === false) {
          $field_name = $this->_uniqueFieldNames[$fld];
          $errors[] = 'Field missing from target site mapping file: "' . $fld . '|' . $field_name. '"".';
        }
      }
    }


    $this->_validated = empty($errors);

    return $this->_validated;
  }

  public function getCiviCrmCustomFields() {

  }


  /**
   * Traverses an array of FieldParsers and returns a one tier list of unique custom CiviCRM field names.
   *
   * @param array $parsers An array of FieldParsers.
   * @return array An array of string custom field names.
   */
  private static function getUniqueCustomFieldNames($parsers) {
    $fields = array();
    foreach($parsers as $parser) {
      $fields = array_merge($fields, $parser->fields);
    }

    // Remove duplicate values.
    $fields = array_unique($fields);
    // Re-index the keys.
    $fields = array_values($fields);
    return $fields;
  }

  /**
   * Loops over all the ModuleParsers and their files to return an array of unique file names.
   *
   * @param array $module_parsers Array of ModuleParser.
   *
   * @return array|void
   */
  private static function getUniqueFileList($module_parsers) {
    if(empty($module_parsers)) {
      return;
    }
    $flat_files_list = array();

    foreach($module_parsers as $module => $parser) {
      $flat_files_list = array_merge($flat_files_list, $parser->files);
    }

    $flat_files_list = array_unique($flat_files_list);
    // Reset index and return files.
    return array_values($flat_files_list);
  }

  /**
   * Validates the required Drush command options.
   * @param array $errors An array of error messages if validation failed.
   *
   * @return bool Returns true if valid, false if not.
   */
  private function validateOptions(&$errors) {
    $valid = true;

    if(!empty($this->sourceDefinitionPath)) {
      if(!is_readable($this->sourceDefinitionPath)) {
        $errors[] = 'Unable to read --source file located at: ' . $this->sourceDefinitionPath . '.';
      }
      else {
        $this->currentSiteCustomFields = include $this->sourceDefinitionPath;
      }
    }

    // Get modules that don't exist.
    $invalid_modules = array_diff_key($this->modules, self::$drupalModules);
    // Filter down the module list to modules we know exist.
    $this->modules = array_diff_assoc($this->modules, $invalid_modules);

    // Check that the file exists and is readable.
    if(!empty($this->targetDefinitionPath) && !is_readable($this->targetDefinitionPath)) {
      $errors[] = 'Unable to read file: ' . $this->targetDefinitionPath. '.';
      $valid = false;
    }

    $this->targetSiteCustomFields = include $this->targetDefinitionPath;

    if(empty($this->targetSiteCustomFields)) {
      $errors[] = 'Unable to load field info from provided mapping file: ' . $this->targetDefinitionPath . '.';
    }

    // Write an error for each module that was not found in the provided list.
    foreach(array_keys($invalid_modules) as $invalid) {
      $errors[] = 'The provided module: ' . $invalid . ' was not found in the current Drupal site.';
      $valid = false;
    }

    return $valid;
  }

  private function validateParsers(&$errors) {
    ModuleParser::$drupalExtensions = self::$drupalModules;
    $validation_errors = array();

    foreach($this->modules as $module => &$value) {
      try{
        $parser = new ModuleParser($module, $this->drupalRoot);
        $parser->parseFiles($validation_errors);
        $value = $parser;
      }
      catch(\Exception $e) {
        $validation_errors[] = $e->getMessage();
      }
    }

    $errors = array_merge($errors, $validation_errors);
    return empty($validation_errors);
  }
}
