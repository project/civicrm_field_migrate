<?php

namespace CiviCrmFieldMigrate\lib;
class CiviCrmFieldExportController {

  public $destinationSrc;
  public $customFields;

  function __construct($destination_src, $custom_fields) {
    $this->destinationSrc = $destination_src;
    $this->customFields = $custom_fields;
  }

  public function execute() {

    $fields_string = var_export($this->customFields, true);
    $file_contents = '<?php

    return ' . $fields_string . ';
    ';

    file_put_contents($this->destinationSrc, $file_contents);
  }


  public static function commandInfo() {
    $items = array(
      'civicrm-field-export' => array(
        'description' => 'Exports CiviCRM\'s custom fields to a file for the civicrm-field-migrate command.',
        'bootstrap' => DRUSH_BOOTSTRAP_NONE,
        'options' => array(
          'destination' => array(
            'example-value' => 'path/source.mapping.php',
            'description' => 'The output file that will contain PHP code used in the civicrm-field-migrate command.',
            'required' => true,
          ),
        ),
        'aliases' => array(
          'cfe'
        ),
        'examples' => array(
          'Sample' => 'drush cfe --destination=./developer-1-source-civi-fields.php',
        ),
        //'core' => '7.x',
        //'callback' => 'civicrm_field_migrate',
        'command-hook' => 'field_export'
      ),
    );

    return $items;
  }

  public static function helpInfo($command) {
    switch($command) {
      case 'drush:civicrm-field-export':
        return 'Exports the current Drupal site\'s CiviCRM custom fields to a file used in the mapping for civicrm-field-migrate command.';
        break;
    }
  }

  public function validate(&$errors) {
    // Make sure the destination doesn't exist.
    if(file_exists($this->destinationSrc)) {
      $errors[] = 'The destination file: ' . $this->destinationSrc . ' already exists.';
      return false;
    }

    // Get just the directory from the destination.
    $dir = preg_replace('/\/[^\/]+$/', '', $this->destinationSrc);
    // Check if the directory is writable.
    if(!is_writeable($dir)) {
      $errors[] = 'The parameter --destination: ' . $this->destinationSrc . ' is not writable.';
    }

    if(empty($this->customFields)) {
      $errors[] = 'There were no CiviCRM custom fields for the current Drupal site.';
    }
    return empty($errors);
  }
}
