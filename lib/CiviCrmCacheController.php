<?php

namespace CiviCrmFieldMigrate\lib;
class CiviCrmCacheController {
  public static function commandInfo() {
    return array(
      'civicrm-cache-rebuild' => array(
        'description' => 'Clears CiviCRM caches and rebuilds the Civi\'s menu and DB generated items.',
        'boostrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
        'examples' => array(
          'Sample' => 'drush ccr'
        ),
        'aliases' => array(
          'ccr'
        ),
      ),
    );
  }

  public static function helpInfo($command) {
    switch($command) {
      case 'drush:civicrm-cache-rebuild':
        return 'Clears all CiviCRM caches and regenerates ORM DB generated items. This is mainly helpful when migrating to a new environment.';
        break;
    }
  }

  public static function execute() {
    civicrm_initialize();
    \CRM_Core_Invoke::rebuildMenuAndCaches(true, true);
  }
}
