<?php
namespace CiviCrmFieldMigrate\lib;

class FieldParser {
  public $fields = array();
  public $file = "";
  private static $_fieldRegExp = '/custom[_\-]\d+/';
  private static $_siteFields;

  function __construct($file, $site_fields) {
    $this->file = $file;
    if(empty(self::$_siteFields)) {
      // We're using a static field here because there will only be one version of the data.
      // It seems silly to have a copy of it for each instance of this class.
      self::$_siteFields = $site_fields;

      // Lets change the data structure a bit so we only need to do it once
      // here and not every time other processes need individual pieces of field info.
      // We'll break the ID out of the "custom_xxx" and store it as the key.

      $new_structure = array();
      foreach(self::$_siteFields as $key => $name) {
        $id = preg_replace('/^[^0-9]+(?=\d+$)/mS', '', $key);
        $new_structure[$id] = $name;
      }

      self::$_siteFields = $new_structure;
    }
  }

  function parseFields(&$errors) {
    if(!is_writable($this->file)) {
      // set and error and return null.
      $errors[] = 'File is not writable: ' . $this->file . '.';
      return false;
    }

    $contents = file_get_contents($this->file);
    $fields = array();

    // Get all instances of "custom_xxx".
    preg_match_all(self::$_fieldRegExp, $contents, $fields);

    if(!empty($fields[0])) {

      // Convert custom-123 version into custom_123
      array_walk($fields[0], function(&$field, $index){
        $field = str_ireplace('-', '_', $field);
      });

      $this->fields = array_values(array_unique($fields[0]));
    }

    // Get all instances of: "field_name_id".
    foreach(self::$_siteFields as $id => $name) {
      $pattern = '/' . str_replace('_', '[\-_]+', $name) . '[\-_]+' . $id . '/i';
      if(preg_match($pattern, $contents) === 1) {
        // The file contains this field, add it to the $fields.
        $this->fields[] = "custom_" . $id;
      }
    }
  }
}
