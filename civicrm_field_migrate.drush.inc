<?php
include_once "lib/FieldParser.php";
include_once "lib/ModuleParser.php";
include_once "lib/CiviCrmFieldMigrateController.php";
include_once "lib/CiviCrmFieldExportController.php";
include_once "lib/CiviCrmCacheController.php";


/**
 * Implements hook_drush_help().
 */
function civicrm_field_migrate_drush_help($command) {
  $results = CiviCrmFieldMigrate\lib\CiviCrmFieldMigrateController::helpInfo($command);
  $results .= CiviCrmFieldMigrate\lib\CiviCrmFieldExportController::helpInfo($command);
  $results .= CiviCrmFieldMigrate\lib\CiviCrmCacheController::helpInfo($command);

  return $results;
}

/**
 * Implements hook_drush_command().
 */
function civicrm_field_migrate_drush_command() {
  return array_merge(
    \CiviCrmFieldMigrate\lib\CiviCrmFieldMigrateController::commandInfo(),
    \CiviCrmFieldMigrate\lib\CiviCrmFieldExportController::commandInfo(),
    \CiviCrmFieldMigrate\lib\CiviCrmCacheController::commandInfo()
  );
}

/**
 * The callback for the civicrm-field-migrate drush command.
 */
function drush_civicrm_field_migrate_field_migrate() {
  if (!drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_FULL)) {
    return drush_set_error('DRUPAL_SITE_NOT_FOUND', dt('You need to specify an alias or run this command within a drupal site.'));
  }
  $errors = array();
  $migrated_fields = array();
  $controller = civicrm_field_migrate_get_controller_singleton();

  // Let the user know nothing has happened if this validation has failed.
  if ($controller->execute($errors, $migrated_fields) === FALSE) {
    foreach ($errors as $error) {
      drush_set_error('failed_validation', $error);
    }
    drush_set_error('failed_validation', 'Failed validation, can not continue. No changes have been made.');
    return FALSE;
  }

  foreach($migrated_fields as $fid => $field) {
    drush_log('Migrated field: ' . $field['name'] . ' from ' . $fid . ' to ' . $field['new_id'] . '.', 'success');
  }

  drush_log(($controller->testOnly ? 'Tested' : 'Finished') . ' migrating modules: ' . implode(', ', array_keys($controller->modules)) . '.', 'success');
  if($controller->testOnly) {
    drush_log('Test only, no files were changed.');
  }

  return true;
}

/**
 * The validate callback for the civicrm-field-migrate drush command.
 *
 * @return bool
 */
function drush_civicrm_field_migrate_field_migrate_validate() {
  if (!drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_FULL)) {
    return drush_set_error('DRUPAL_SITE_NOT_FOUND', dt('You need to specify an alias or run this command within a drupal site.'));
  }
  $controller = civicrm_field_migrate_get_controller_singleton();

  $errors = array();
  // Let the user know nothing has happened if this validation has failed.
  if (!$controller->validate($errors)) {
    foreach ($errors as $error) {
      drush_set_error('failed_validation', $error);
    }
    drush_set_error('failed_validation', 'Failed validation, can not continue. No changes have been made.');
    return FALSE;
  }

  return TRUE;
}

/**
 * The callback for the civicrm-field-export drush command.
 */
function drush_civicrm_field_migrate_field_export() {
  if (!drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_FULL)) {
    return drush_set_error('DRUPAL_SITE_NOT_FOUND', dt('You need to specify an alias or run this command within a drupal site.'));
  }
  $controller = civicrm_field_migrate_get_exported_controller_singleton();
  $controller->execute();
  drush_log("Successfully exported CiviCRM custom fields to: " . $controller->destinationSrc . ".", 'success');
}

/**
 * The callback for the civicrm-cache-rebuild drush command.
 */
function drush_civicrm_field_migrate_civicrm_cache_rebuild() {
  CiviCrmFieldMigrate\lib\CiviCrmCacheController::execute();
  drush_log('Cleared CiviCRM caches and rebuilt generated files.', 'success');
}

/**
 * The validation callback for the civicrm-field-export drush command.
 */
function drush_civicrm_field_migrate_field_export_validate() {
  if (!drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_FULL)) {
    return drush_set_error('DRUPAL_SITE_NOT_FOUND', dt('You need to specify an alias or run this command within a drupal site.'));
  }
  $controller = civicrm_field_migrate_get_exported_controller_singleton();

  $errors = array();
  // Let the user know nothing has happened if this validation has failed.
  if (!$controller->validate($errors)) {
    foreach ($errors as $error) {
      drush_set_error('failed_validation', $error);
    }
    drush_set_error('failed_validation', 'Failed validation, can not continue. No changes have been made.');
    return FALSE;
  }

  return TRUE;

}

/**
 * The validation callback for the civicrm-cache-rebuild drush command.
 */
function drush_civicrm_field_migrate_civicrm_cache_rebuild_validate() {
  // There is nothing to validate. Assume true.
  return true;
}

/**
 * Gets array of custom fields in the current Drupal site keyed on the custom_xxx format
 * with the value being the machine name.
 *
 * @return array;
 */
function civicrm_field_migrate_get_site_custom_fields() {
  $results = drush_db_select("civicrm_custom_field", array(
    "id",
    "name"
  ))->fetchAllKeyed();

  foreach ($results as $id => $name) {
    $custom_fields['custom_' . $id] = $name;
  }

  return $custom_fields;
}

/**
 * Instantiates or retrieves the statically stored CiviCrmFieldMigrateController.
 *
 * @return \CiviCrmFieldMigrate\lib\CiviCrmFieldMigrateController
 */
function civicrm_field_migrate_get_controller_singleton() {
  static $controller;

  if (!$controller) {
    $target_def_src = drush_get_option('target');
    $source_def_src = drush_get_option('source');
    $modules = drush_get_option_list('modules');
    $test_only = drush_get_option('test-only');

    \CiviCrmFieldMigrate\lib\CiviCrmFieldMigrateController::$drupalModules = drush_get_extensions();

    $controller = new \CiviCrmFieldMigrate\lib\CiviCrmFieldMigrateController(
      $modules,
      $target_def_src,
      DRUPAL_ROOT,
      (!empty($source_def_src) ? NULL : civicrm_field_migrate_get_site_custom_fields()),
      $source_def_src,
      !empty($test_only));
  }

  return $controller;
}

function civicrm_field_migrate_get_exported_controller_singleton() {
  static $controller;
  if (!$controller) {
    $destination = drush_get_option('destination');
    $controller = new \CiviCrmFieldMigrate\lib\CiviCrmFieldExportController(
      $destination,
      civicrm_field_migrate_get_site_custom_fields());
  }

  return $controller;
}

