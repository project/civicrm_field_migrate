# CiviCRM Custom Field Migrate

**This is a Drush tool, not a module to be installed on your site.**

I needed to be able to deploy our exported features and some hard coded CiviCRM custom fields to different environments like production or QA (or even another developer's work machine).
The problem is that CiviCRM uses an incremental ID to construct its custom field names, i.e.: custom_100. They do provide a machine name style value in the database but it's not really used.
For this reason, I created a tool that would allow me to make a field export process that would generate a file of all custom fields in each environment I need to deploy to. 

This tool replaces the occurrences of custom_xxx or field_name_id with the target environment's equivalent  by matching the target environments exported fields via the field "name".

Another handy feature is the validation process! This tool will verify the custom field integrity for the current code base against the target environment's fields. 
It will warn you that your code references custom fields that aren't in the target environments exported fields. 

**Be warned! This tool alters the modules' files you tell it to scan.**

_Please make sure your code is under version control or you have backups._

This tool has two commands:

 * civicrm-field-migrate
     * Migrates your provided list of modules' code to use CiviCRM custom field names from the target CiviCRM site via the exported field definition that can be created by `civicrm-field-export`. 
     * Example: `drush cfm --modules="my_exported_feature, my_civi_feature" --target="${PWD}/prod.civi.fields.php"`
         * Provide the `--test-only` option to only validate the code against the target environment's field definition. **No files are modified.**
         * Instead of using the current site's field definition, use one exported from another site: `drush cfm --modules="my_exported_feature, my_civi_feature" --source="/some/path/source.fields.php" --target="${PWD}/prod.civi.fields.php"`
 * civicrm-field-export
     * Exports all CiviCRM custom fields to a file to be used as the target migration in `civicrm-field-migrate`.
     * Example: `drush cfe --destination="${PWD}/prod.civi.fields.php"`


## Installation

This doesn't need to be installed into a site, but rather your .drush folder.

 1. `drush @none dl civicrm-field-migrate`. The `@none` will install it to your .drush directory.
 2. `drush cc drush`
 3. Create a field export of the environment you intend to deploy to: `drush cfe --destination="${PWD}/prod.civi.fields.php"`
 4. Migrate an environment to the target environment by using your newly created field definition: `drush cfm --modules="my_exported_feature, my_second_civi_module" --target="/path/to/field/def/prod.civi.fields.php"`


